﻿namespace LFSR
{
    public class LinearFeedShiftRegister
    {
        private ulong Register { get; set; }
        public int StreamSize { get; private set; }

        public LinearFeedShiftRegister(ulong? seed, int streamSize = sizeof(ulong) * 8)
        {
            if (seed != null) Register = (ulong) seed;
            else Register = 17; // determined by a fair die roll.
            StreamSize = streamSize;
        }

        private ulong AdvanceRegister()
        {
            var output = Register & 1;
            var newBit = (output ^ (Register >> 1)) & 1;
            Register = (Register >> 1) | (newBit << ((sizeof(ulong) * 8) - 1));
            return output;
        }

        public ulong GenerateBitStream()
        {
            ulong output = 0;
            for (int i = 0; i < StreamSize; i++)
            {
                output |= (AdvanceRegister() << i++);
            }
            return output;
        }

        public void SetNewSeed(ulong newSeed, int streamSize = sizeof(ulong) * 8)
        {
            Register = newSeed;
            StreamSize = streamSize;
        }
    }
}