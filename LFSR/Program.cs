﻿using System;

namespace LFSR
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            LinearFeedShiftRegister myShiftRegister = new(0xDEADBEEF);
            var output = myShiftRegister.GenerateBitStream();
            
            Console.WriteLine($"Generated the value {output}.");
            
            // myShiftRegister.SetNewSeed(output);
            myShiftRegister = new LinearFeedShiftRegister(output);
            Console.WriteLine("Passing output back into LFSR. . .");
            Console.WriteLine($"Generated the value {myShiftRegister.GenerateBitStream()}.");
        }
    }
}